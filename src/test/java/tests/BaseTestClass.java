package tests;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import pageobjects.HomePage;
import pageobjects.WomenPage;
import utils.ReadProperties;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

public class BaseTestClass {

    protected WebDriver driver;
    protected HomePage homePage;
    protected WomenPage womenPage;
    protected final Logger log = LogManager.getLogger(BaseTestClass.class);

    @Before
    public void setup() throws IOException {
        // Traditional Way
        //System.setProperty("webdriver.chrome.driver", "drivers/macos/chromedriver");

        // Modern Way - browser setup
        WebDriverManager.chromedriver().setup();
        driver = new ChromeDriver();
        log.info("Browser was successfully launched!");

        driver.manage().window().maximize();
        log.info("Window was successfully maximized!");

        //driver.get("http://automationpractice.com/index.php");
        goToUrl(ReadProperties.getConfigProperties("browserUrl"));

        // Page Objects initialisation - will be initialized before running the Tests
        homePage = new HomePage(driver);
        womenPage = new WomenPage(driver);

        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
    }

    @After
    public void tearDown() {
        driver.quit();
        log.info("Browser was successfully closed!");
    }

    private void goToUrl(String url) {
        driver.get(url);
    }

    protected String getCurrentUrl() {
        return driver.getCurrentUrl();
    }

    protected void refreshPage() {
        driver.navigate().refresh();
    }
}
