package tests;

import org.junit.Assert;
import org.junit.Test;
import utils.ReadProperties;

import java.io.IOException;

public class FirstTest extends BaseTestClass {

    @Test
    public void firstAutomationTest() throws IOException {
        String actualTitle = driver.getTitle();
        String expectedTitleFromPropertyFile = ReadProperties.getTestDataProperties("pageTitle");

        Assert.assertEquals(expectedTitleFromPropertyFile, actualTitle);
        System.out.println("Print Top Menu Elements: " + homePage.getTopMenuElements());

        log.info("First test passed!");
    }

    @Test
    public void checkTotalNrOfProducts() {
        homePage.clickOnWomenButton();
        int actualNrOfProducts = womenPage.returnTotalNrOfProducts();

        Assert.assertEquals(7, actualNrOfProducts);

        log.info("Second test passed!");
    }
}
