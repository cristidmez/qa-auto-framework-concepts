package utils;

import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.Random;

public class Helper {

    private static final Random random = new Random();

    public static int generateRandomNumber(int maxValue) {
        return random.nextInt(maxValue);
    }

    public static int generateRandomNumberWithBoundaries(int min, int max) {
        return random.nextInt(max - min) + min;
    }

    public static String getLocalDateTimeGmtFormat() {
        return LocalDateTime.now(ZoneOffset.UTC).format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm"));
    }

    public File createNewFile(String fileName) throws IOException {
        String fileLocation = System.getProperty("user.dir") + "/";
        File file = new File(fileLocation, fileName);
        file.createNewFile();

        return file;
    }

}
