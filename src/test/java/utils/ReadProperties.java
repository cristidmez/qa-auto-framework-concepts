package utils;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class ReadProperties {

    public static String getConfigProperties(String propertyName) throws IOException {
        // FIRST VERSION
        //FileInputStream file = new FileInputStream("src/test/resources/configuration.properties");
        FileInputStream file = new FileInputStream(System.getProperty("user.dir") + "/src/test/resources/configuration.properties");

        // SECOND VERSION - It's doing the same thing as above - BUT you need to remove the static keyword from the method
        // To read files from the classpath -> create Input stream object for the properties file
        //InputStream file = getClass().getClassLoader().getResourceAsStream("configuration.properties");

        // create Properties class object / instance to access properties file
        Properties prop = new Properties();

        // load the properties file
        prop.load(file);

        System.out.println("Config Property value: " + prop.getProperty(propertyName));

        return prop.getProperty(propertyName);
    }

    public static String getTestDataProperties(String propertyName) throws IOException {
        // FIRST VERSION
        //FileInputStream file = new FileInputStream("src/test/resources/configuration.properties");
        FileInputStream file = new FileInputStream(System.getProperty("user.dir") + "/src/test/resources/test-data.properties");

        // SECOND VERSION - It's doing the same thing as above - BUT you need to remove the static keyword from the method
        // To read files from the classpath -> create Input stream object for the properties file
        //InputStream file = getClass().getClassLoader().getResourceAsStream("configuration.properties");

        // create Properties class object / instance to access properties file
        Properties prop = new Properties();

        // load the properties file
        prop.load(file);

        System.out.println("Test Data Property value: " + prop.getProperty(propertyName));

        return prop.getProperty(propertyName);
    }
}
