package pageobjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.FindBys;
import org.openqa.selenium.support.PageFactory;

import java.util.List;
import java.util.stream.Collectors;

public class HomePage extends BasePageObject {

    // Non Page Factory - Classic way
    By topMenuList = By.cssSelector("#block_top_menu >ul > li");
    By womenTopMenuButton = new By.ByCssSelector("#block_top_menu a[title=Women]");

    // Page Factory way
    @FindBy(css = "#block_top_menu >ul > li")
    List<WebElement> topMenuListPF;

    @FindBy(css = "#block_top_menu a[title=Women]")
    WebElement womenTopMenuButtonPF;

    public HomePage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }

    public List<String> getTopMenuElements() {
        //List<WebElement> menuElements = driver.findElements(topMenuListPF);
        return topMenuListPF.stream()
                .map(WebElement::getText)
                .collect(Collectors.toList());
    }

    public void clickOnWomenButton() {
        // NON Page Factory
        //driver.findElement(womenTopMenuButton).click();

        // Page Factory way
        womenTopMenuButtonPF.click();
    }
}
