package pageobjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class WomenPage extends BasePageObject {

    // Non Page Factory - Classic way
    By totalNrOfProducts = new By.ByCssSelector(".heading-counter");

    // Page Factory
    @FindBy(css = ".heading-counter")
    WebElement totalNrOfProductsBF;

    public WomenPage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }

    public int returnTotalNrOfProducts() {
        //String totalNrText = driver.findElement(totalNrOfProducts).getText();
        String totalNrText = totalNrOfProductsBF.getText();
        int totalNrOfProducts = Integer.parseInt(totalNrText.replaceAll("[^0-9]", ""));
        System.out.println("Total Nr. of Products is: " + totalNrOfProducts);
        return totalNrOfProducts;
    }
}
